# Aprende

Luan Einhardt - ldseinhardt@gmail.com

Créditos do algoritmo de recomendação ao Henrique Lemos.

## Marmotta

### Instalação por docker

https://github.com/nicholsn/docker-marmotta

### Tutoriais

http://marmotta.apache.org/events/iswc2014.html

https://github.com/wikier/apache-marmotta-tutorial-iswc2014

## Base de dados OER Commons

[OER Commons](https://www.oercommons.org)

## Para por em produção

- Fazer a instalação do marmotta
- Configurar a base de dados MySQL (pasta `/database`)
- Enviar para o servidor os arquivos da pasta `/back-end`
- Criar as pastas de cache (`temp/cache/http_cache` e `temp/cache/twig`)
- Enviar também as dependências ou então executar o comando `composer install` no servidor
- Realizar as configurações em `/back-end/config/configs.php`
- carragar os rdfs no marmotta (necessário a pasta `/docs`), para isso acessar `http://localhost/rdfs` para que os mesmos sejam carregados
- configurar o apache ou nginx caso o Marmotta fique no mesmo servidor

### Configurar a base de dados MySQL

```bash
$ mysql -u root -p
> source database/create.sql
```

### Instalação do composer

[Composer](https://getcomposer.org)

```bash
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ sudo php composer-setup.php --install-dir=/bin --filename=composer
```

### Configurações (`/back-end/config/configs.php`)

```php
$database = [
  'driver'   => 'pdo_mysql',
  'charset'  => 'utf8',
  'host'     => 'localhost',
  'dbname'   => 'aprende',
  'username' => 'root',
  'password' => 'test123'
];

$marmotta = [
    'url' => 'http://localhost:8080/marmotta',
    'username' => 'admin',
    'password' => 'pass123'
];
```

### Instalação das dependências

```bash
$ cd back-end
$ composer install
```

### Dependências do back-end (`composer.json`)

```json
{
  "require" : {
    "doctrine/dbal": "~2.5",
    "guzzle/http": "^3.9",
    "silex/silex": "~2.0",
    "swiftmailer/swiftmailer": "^5.4",
    "symfony/twig-bridge": "^3.0",
    "symfony/validator": "^3.0",
    "twig/twig": ">=1.8.0,<2.0-dev",
    "cocur/slugify": "^2.3"
  }
}
```

### PHP Framework utilizado

[Silex](http://silex.sensiolabs.org/)

### Para testar localmente com o servidor do php (php >= 5.4)

```bash
$ ./run
```

`Nota: Instalar o php e seus modulos como php-curl ...`

```bash
$ sudo apt-get update
$ sudo apt-get install -y php7 php7-*
```

## Front-end

Para modificar a interface, realizar as modificações dentro da pasta `/front-end`

Para gerar o front-end, será necessário as ferramentas:

- NPM (gerênciador de dependências do node.js)
- GULP (Task runner)

### Instalação do npm e gulp

```bash
$ sudo apt-get -y update
$ sudo apt-get -y install npm nodejs
$ sudo npm install -g gulp-cli
```

### Instalação das dependências

```bash
$ cd front-end
$ npm install
```

### Dependências do front-end (`package.json`)

- [Bootstrap](http://getbootstrap.com/)
- [JQuery](https://jquery.com/)
- [Font-Awesome](http://fontawesome.io/)
- [Mark.js](https://markjs.io/)

### Gerando o `front-end` em `back-end`

O Gulp realizará todas as tarefas descritas no `gulpfile.js`, como:

- Copiar dependências
- Concatenar arquivos
- Compilar less para css
- Minificar arquivos css, js e html
- Otimizar a qualidade de imagens

```
$ cd front-end
$ gulp
```

Notas: O Endpoint de consultas SPARQL do Marmotta se mostrou extremamente ruim, talvez pela versão ou por estar sobre um container docker. Perdendo diversas vezes a conexão interna com a base de dados, a solução por hora foi reiniciar o container docker (as vezes funciona) ou então remover o container docker e criar uma nova instância (felizmente com docker isso é rápido, não mais que 3 ou 4 comandos). Além disso, o Marmotta apresentou problemas com a codificação de caracteres especiais.
