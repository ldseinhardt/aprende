<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

$debug = true;

$database = [
  'driver'   => 'pdo_mysql',
  'charset'  => 'utf8',
  'host'     => 'localhost',
  'dbname'   => 'aprende',
  'username' => 'root',
  'password' => 'test123'
];

$marmotta = [
    'url' => 'http://localhost:8080/marmotta',
    'username' => 'admin',
    'password' => 'pass123'
];

// UTC date
// Brazil/Brasilia -3h
// Equador/Guayaquil -5h
date_default_timezone_set('UTC');
