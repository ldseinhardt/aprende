<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class Aprende implements ControllerProviderInterface
{

    /**
    * Add routes
    */
    public function connect(Application $app)
    {
        // Create new controller route
        $routing = $app['controllers_factory'];

        // Add controller routes
        Controllers\Main::addRoutes($routing);
        Controllers\User::addRoutes($routing);
        Controllers\View::addRoutes($routing);
        Controllers\Recommender::addRoutes($routing);

        return $routing;
    }
}
