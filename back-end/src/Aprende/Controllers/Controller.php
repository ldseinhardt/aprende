<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende\Controllers;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class Controller
{

    /**
    * Return session auth
    */
    protected function auth(Application $app)
    {
        return $app['session']->get('auth');
    }

    /**
    * Validade
    */
    protected function validate($validator, $data, $collection)
    {
        $constraint = new Assert\Collection($collection);

        $errors = $validator->validate($data, $constraint);

        $response = [
            'status' => true
        ];

        if (count($errors) > 0) {
            $response['status'] = false;
            $response['message'] = 'Validate error.';
            foreach ($errors as $error) {
                $field = preg_replace('/\[|\]/', '', $error->getPropertyPath());
                $response['errors'][$field] = $error->getMessage();
            }
        }

        return $response;
    }

}
