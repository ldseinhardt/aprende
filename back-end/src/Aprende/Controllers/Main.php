<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende\Controllers;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use Aprende\Models;

class Main extends Controller
{

    /**
    * Add routes
    */
    public static function addRoutes($routing)
    {
        $routing->get('/', [new self(), 'index'])
            ->bind('main.index');

        $routing->get('/search', [new self(), 'search'])
            ->bind('main.search');

        $routing->get('/object', [new self(), 'object'])
            ->bind('main.object');

        $routing->get('/rdfs', [new self(), 'rdfs'])
            ->bind('main.rdfs');
    }

    /**
     * Search home
     */
    public function index(Application $app)
    {
        return $app['twig']->render('Aprende/Views/Main/index.html', [
            'subjects' => $this->getSubjects(),
            'userLogged' => $this->auth($app)
        ]);
    }

    /**
     * Search results
     */
    public function search(Application $app)
    {
        // Request data
        $request = $app['request'];

        $data = [
            'search' => $app->escape($request->get('search')),
            'subject' => $app->escape($request->get('subject')),
            'offset' => $app->escape($request->get('offset', 0)),
            'limit' => $app->escape($request->get('limit', 10))
        ];

        if (!$data['search']) {
            return $app->redirect('/');
        }

        $marmotta = new Models\Marmotta($app);

        return $app['twig']->render('Aprende/Views/Main/search.html', [
            'userLogged' => $this->auth($app),
            'search' => $data,
            'total' => $marmotta->searchCount($data['search']),
            'results' => $marmotta->search($data['search'], $data['offset'], $data['limit'])
        ]);
    }

    /**
     * Result (object) view
     */
    public function object(Application $app)
    {
        // Request data
        $request = $app['request'];

        $object_uri = $app->escape($request->get('object'));

        if (!$object_uri) {
            return $app->redirect('/');
        }

        $auth = $this->auth($app);

        if ($auth) {
            $app['session']->remove('redirect');
        } else {
            $app['session']->set('redirect', '/object?object=' . $object_uri);
            return $app->redirect('/login');
        }

        $marmotta = new Models\Marmotta($app);

        return $app['twig']->render('Aprende/Views/Main/object.html', [
            'userLogged' => $auth,
            'object_uri' => $object_uri,
            'result' => $marmotta->searchObject($object_uri)
        ]);
    }

    /**
     * Insert rdfs in Marmotta
     */
    public function rdfs(Application $app)
    {
        $marmotta = new Models\Marmotta($app);
        $marmotta->insertRDFS();
        return $app->json('OK');
    }

    /**
     * Return subjects
     */
    private function getSubjects()
    {
        return [
            ['value' => 'applied-science', 'text' => 'Applied Science'],
            ['value' => 'arts-and-humanities', 'text' => 'Arts and Humanities'],
            ['value' => 'business-and-communication', 'text' => 'Business and Communication'],
            ['value' => 'career-and-technical-education', 'text' => 'Career and Technical Education'],
            ['value' => 'education', 'text' => 'Education'],
            ['value' => 'english-language-arts', 'text' => 'English Language Arts'],
            ['value' => 'history', 'text' => 'History'],
            ['value' => 'law', 'text' => 'Law'],
            ['value' => 'life-science', 'text' => 'Life Science'],
            ['value' => 'mathematics', 'text' => 'Mathematics'],
            ['value' => 'physical-science', 'text' => 'Physical Science'],
            ['value' => 'social-science', 'text' => 'Social Science'],
        ];
    }

}
