<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 * Recommender by Henrique Lemos
 */

namespace Aprende\Controllers;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use Aprende\Models;

class Recommender extends Controller
{

    /**
    * Add routes
    */
    public static function addRoutes($routing)
    {
        $routing->get('/recommender', [new self(), 'recommender'])
            ->bind('recommender');
    }

    /**
     * Recommender objects
     */
    public function recommender(Application $app)
    {
        // Request data
        $request = $app['request'];

        $auth = $this->auth($app);

        if (!$auth) {
            return $app->json(['status' => false]);
        }

        $recommends = $this->contentBasedRecommend($app, $auth['id']);

        $marmotta = new Models\Marmotta($app);

        $recommends = array_map(function($e) use($marmotta) {
            return $marmotta->getTitleAndImage($e);
        }, $recommends);

        if (count($recommends) > 0) {
            return $app->json([
                'status' => true,
                'data' => $recommends
            ]);
        }

        return $app->json(['status' => false]);
    }

    /**
     * Recommender content based
     */
    private function contentBasedRecommend(Application $app, $user_id, $numrecs = 10)
    {
        $view = new Models\View($app['db'], [
            'user_id' => $user_id
        ]);

        // object with views
        $objects = $view->getObjects();

        if (!count($objects)) {
            //return objects with the most views
            return $view->getTopObjects($numrecs);
        }

        // categories with views
        $categories = $view->getCategories();

        // search in marmotta numrecs objects where categories
        $marmotta = new Models\Marmotta($app);

        $marmotta_recs = $marmotta->getObjectsByCategory($categories, $numrecs);

        // return views
        $recs = $view->getViews($marmotta_recs);

        // remove user views
        $recs = array_filter($recs, function($key) use($objects) {
            return !in_array($key, $objects);
        }, ARRAY_FILTER_USE_KEY);

        // sort
        arsort($recs);

        // retorna somente os ids
        return array_keys($recs);
    }

}
