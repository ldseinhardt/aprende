<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende\Controllers;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use Aprende\Models;

class User extends Controller
{

    /**
    * Add routes
    */
    public static function addRoutes($routing)
    {
        $routing->match('/register', [new self(), 'register'], 'GET|POST')
            ->bind('user.register');

        $routing->match('/login', [new self(), 'login'], 'GET|POST')
            ->bind('user.login');

        $routing->match('/logout', [new self(), 'logout'], 'GET|POST')
            ->bind('user.logout');

        //$routing->match('/user/password/reset/', [new self(), 'resetPassword'], 'GET|POST')
        //    ->bind('user.password_reset');

        //$routing->match('/user/password/new/', [new self(), 'newPassword'], 'GET|POST')
        //    ->bind('user.password_new');
    }

    /**
    * Register new user
    */
    public function register(Application $app)
    {
        if ($this->auth($app)) {
            return $app->redirect('/');
        }

        // Request data
        $request = $app['request'];

        $data = [
            'first_name'       => $app->escape($request->get('first_name', '')),
            'last_name'        => $app->escape($request->get('last_name', '')),
            'email'            => $app->escape($request->get('email', '')),
            'password'         => $app->escape($request->get('password', '')),
            'confirm_password' => $app->escape($request->get('confirm_password', ''))
        ];

        $response = null;

        if ($request->isMethod('POST')) {
            // Validade
            $response = $this->validate($app['validator'], $data, [
                'first_name' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 2,
                        'max' => 32
                    ])
                ],
                'last_name' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4,
                        'max' => 64
                    ])
                ],
                'email' => [
                    new Assert\NotBlank(),
                    new Assert\Email()
                ],
                'password' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4
                    ]),
                    new Assert\EqualTo([
                        'value' => $data['confirm_password']
                    ])
                ],
                'confirm_password' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4
                    ])
                ]
            ]);

            if ($response['status']) {
                // Add user
                $user = new Models\User($app['db'], $data);
                $id = $user->add();
                if ($id > 0) {
                    $app['session']->set('auth', [
                        'id' => $id,
                        'first_name' => $data['first_name'],
                        'last_name' => $data['last_name'],
                        'email' => $data['email']
                    ]);
                    $redirect = $app['session']->get('redirect');
                    return $app->redirect($redirect ? $redirect : '/');
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Registered email.',
                        'errors' => []
                    ];
                }
            }
        }

        return $app['twig']->render('Aprende/Views/User/register.html', [
            'data' => $data,
            'response' => $response
        ]);
    }

    /**
    * Create session
    */
    public function login(Application $app)
    {
        if ($this->auth($app)) {
            return $app->redirect('/');
        }

        // Request data
        $request = $app['request'];

        $data = [
            'email'    => $app->escape($request->get('email', '')),
            'password' => $app->escape($request->get('password', ''))
        ];

        $response = null;

        if ($request->isMethod('POST')) {
            // Validade
            $response = $this->validate($app['validator'], $data, [
                'email' => [
                    new Assert\NotBlank(),
                    new Assert\Email()
                ],
                'password' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 4
                    ])
                ]
            ]);

            if ($response['status']) {
                // Auth user
                $user = new Models\User($app['db'], $data);

                $auth = $user->auth();

                if ($auth) {
                    $app['session']->set('auth', $auth);
                    $redirect = $app['session']->get('redirect');
                    return $app->redirect($redirect ? $redirect : '/');
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Invalid credentials.',
                        'errors' => []
                    ];
                }
            }
        }

        return $app['twig']->render('Aprende/Views/User/login.html', [
            'data' => $data,
            'response' => $response
        ]);
    }

    /**
    * Remove session
    */
    public function logout(Application $app)
    {
        // Request data
        $request = $app['request'];

        $redirect = $app->escape($request->get('redirect', '/'));

        $app['session']->remove('auth');

        return $app->redirect($redirect);
    }

    /**
    * Reset password and send email to save new password
    * Require SMTP
    */
    public function resetPassword(Application $app)
    {
        //...
    }

    /**
    * Save new password
    * Require SMTP
    */
    public function newPassword(Application $app)
    {
        //...
    }

}
