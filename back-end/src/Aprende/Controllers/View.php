<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende\Controllers;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use Aprende\Models;

class View extends Controller
{

    /**
    * Add routes
    */
    public static function addRoutes($routing)
    {
        $routing->post('/view', [new self(), 'add'])
            ->bind('view.add');

        $routing->get('/view', [new self(), 'get'])
            ->bind('view.get');
    }

    /**
    * Add view
    */
    public function add(Application $app)
    {
        // Request data
        $request = $app['request'];

        $data = [
            'object_id' => $app->escape($request->get('object_id')),
            'categories' => $app->escape($request->get('categories'))
        ];

        $auth = $this->auth($app);

        if (!$auth) {
            return $app->json(['status' => false]);
        }

        // Validade
        $response = $this->validate($app['validator'], $data, [
            'categories' => [
                new Assert\NotBlank()
            ],
            'object_id' => [
                new Assert\NotBlank()
            ]
        ]);

        if (!$response['status']) {
            return $app->json($response);
        }

        $data['user_id'] = $auth['id'];

        $view = new Models\View($app['db'], $data);
        $view->add();

        $category = new Models\Category($app['db'], $data);

        $categories = explode(', ', $data['categories']);
        foreach ($categories as $_category) {
            $category->add([
                'category' => $_category,
                'object_id' => $data['object_id']
            ]);
        }

        return $app->json($response);
    }

    /**
     * Return views from object
     */
    public function get(Application $app)
    {
        // Request data
        $request = $app['request'];

        $data = [
            'object_id' => $app->escape($request->get('object_id'))
        ];

        // Validade
        $response = $this->validate($app['validator'], $data, [
            'object_id' => [
                new Assert\NotBlank()
            ]
        ]);

        if (!$response['status']) {
            return $app->json($response);
        }

        $view = new Models\View($app['db'], $data);

        $response['data'] = $view->get();

        return $app->json($response);
    }

}
