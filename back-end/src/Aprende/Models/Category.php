<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 * Recommender by Henrique Lemos
 */

namespace Aprende\Models;

use Silex\Application;

class Category extends Model
{

    /**
    * Inicialize class
    */
    function __construct($db, $data = null)
    {
        parent::__construct($db, 'categories_objects', [
            'category'   => null,
            'object_id' => null
        ]);

        $this->setData($data);
    }

    /**
    * Add
    */
    public function add($data = null)
    {
        $this->setData($data);

        if (!$this->contains()) {
            $this->db->insert($this->table, $this->data);
        }

        return $this;
    }

    /**
    * Check category
    */
    private function contains()
    {
        $query = $this->db->fetchAssoc("
            SELECT
                object_id
            FROM
                categories_objects
            WHERE
                category = :category and object_id = :object_id
        ", $this->data);
        return $query !== false;
    }

}
