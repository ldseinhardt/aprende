<?php

namespace Aprende\Models;

use Silex\Application;
use Guzzle\Http\Client;
use Guzzle\Http\Message\BadResponseException;
use Cocur\Slugify\Slugify;

class Marmotta
{

    /**
     * Settings
     */
     private $config;

     /**
      * Service Urls
      */
     private static $PATH_QUERY_SERVICE  = '/sparql/select';
     private static $PATH_UPDATE_SERVICE = '/sparql/update';
     private static $PATH_RESOURCE_SERVICE = '/resource';

    /**
    * Inicialize class
    */
    function __construct(Application $app)
    {
        $this->config = $app['marmotta_config'];
    }

    /**
     * Return total objects to pagination
     */
    public function searchCount($search)
    {
        $query = $this->select('
            PREFIX lom:     <http://ltsc.ieee.org/rdf/lomv1p0/lom#>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT (count(distinct ?object) as ?count) WHERE {
              ?object a lom:LearningObject ;
                      dcterms:title ?title ;
                      dcterms:description ?description
              FILTER (regex(?title, "' . $search . '", "i") || regex(?description, "' . $search . '", "i"))
            }
        ');

        return $query && $query[0]['count']
            ? (int) $query[0]['count']['value']
            : 0;
    }

    /**
     * Search query
     */
    public function search($search, $offset = 0, $limit = 10)
    {
        return $this->select('
            PREFIX lom:     <http://ltsc.ieee.org/rdf/lomv1p0/lom#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX aprende: <http://www.igualproject/rdf/aprende#>

            SELECT * WHERE {
              ?object a lom:LearningObject ;
                      dcterms:title ?title ;
                      dcterms:description ?description ;
                      aprende:thumb ?thumbnail
              FILTER (regex(?title, "' . $search . '", "i") || regex(?description, "' . $search . '", "i"))
            }
            ORDER BY ?title
            OFFSET ' . (string) $offset . '
            LIMIT ' . (string) $limit . '
        ');
    }

    /**
     * Return title and thumbnail from object to recommender
     */
    public function getTitleAndImage($uri)
    {
        $resource = $this->select('
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX aprende: <http://www.igualproject/rdf/aprende#>

            SELECT ?title ?thumb WHERE {
              <' . $uri . '> dcterms:title ?title ;
                             aprende:thumb ?thumb
            }
        ');

        if (!($resource && count($resource))) {
            return [];
        }

        return [
            'object' => $uri,
            'title' => $resource[0]['title']['value'],
            'thumb' => preg_match("/^http/", $resource[0]['thumb']['value'])
                ? $resource[0]['thumb']['value']
                : ''
        ];
    }

    /**
     * Return all data from object
     */
    public function searchObject($uri)
    {
        $resource = $this->select('
            SELECT * WHERE {
                <' . $uri . '> ?property ?object
            }
        ');

        $prefix = [
            'lom'     => 'http://ltsc.ieee.org/rdf/lomv1p0/lom#',
            'dcterms' => 'http://purl.org/dc/terms/',
            'aprende' => 'http://www.igualproject/rdf/aprende#',
            'vcard'   => 'http://www.w3.org/2001/vcard-rdf/3.0#',
            'lomvoc'  => 'http://ltsc.ieee.org/rdf/lomv1p0/vocabulary#'
        ];

        $unique = [
            'http://purl.org/dc/terms/title',
            'http://purl.org/dc/terms/description',
            'http://purl.org/dc/terms/language',
            'http://www.igualproject/rdf/aprende#thumb',
            'http://www.igualproject/rdf/aprende#visits',
            'http://www.igualproject/rdf/aprende#saves',
            'http://www.igualproject/rdf/aprende#tags'
        ];

        $multiple = [
            'http://ltsc.ieee.org/rdf/lomv1p0/lom#educationalContext',
            'http://purl.org/dc/terms/type',
            'http://purl.org/dc/terms/format'
        ];

        $classification = 'http://ltsc.ieee.org/rdf/lomv1p0/lom#classification';

        $contribution = 'http://ltsc.ieee.org/rdf/lomv1p0/lom#lifeCycleContribution';

        $data = [
            'title' => '',
            'description' => '',
            'language' => '',
            'thumb' => '',
            'visits' => 0,
            'saves' => 0,
            'tags' => 0,
            'educationalContext' => [],
            'type' => [],
            'format' => [],
            'subjects' => [],
            'author' => [],
            'publisher' => ''
        ];

        foreach ($resource as $row) {
            preg_match("/([^\/|#]*$)/", $row['property']['value'], $property);
            $property = $property[0];
            if (in_array($row['property']['value'], $unique)) {
                $data[$property] = $row['object']['value'];
            } else if (in_array($row['property']['value'], $multiple) && !in_array($row['object']['value'], $data[$property])) {
                $data[$property][] = $row['object']['value'];
            } else if ($row['property']['value'] == $classification) {
                $temp = $this->select('
                    SELECT * WHERE {
                        <' . $row['object']['value'] . '> ?property ?object
                    }
                ');
                if (!in_array($temp[0]['object']['value'], $data['subjects'])) {
                    $data['subjects'][] = $temp[0]['object']['value'];
                }
            } else if ($row['property']['value'] == $contribution) {
                $temp1 = $this->select('
                    SELECT * WHERE {
                        <' . $row['object']['value'] . '> ?property ?object
                    }
                ');
                $temp2 = $this->select('
                    SELECT * WHERE {
                        <' . $temp1[0]['object']['value'] . '> ?property ?object
                    }
                ');
                if (preg_match('/author/', $temp1[1]['object']['value'])) {
                    $data['author'][] = $temp2[0]['object']['value'];
                } else {
                    $data['publisher'] = $temp2[0]['object']['value'];
                }
            }
        }

        $oercommons_courses = 'https://www.oercommons.org/courses/';
        $slugify = new Slugify();
        $data['resource'] = $oercommons_courses . $slugify->slugify($data['title']);
        $data['resource_view'] = $data['resource'] . '/view';

        return $data;

        /*
        resource endpoint (lento)

        $oercommons_courses = 'https://www.oercommons.org/courses/';
        $slugify = new Slugify();
        $data['resource'] = $oercommons_courses . $slugify->slugify($data['title']);
        $data['resource_view'] = $data['resource'] . '/view';

        print_r($data);

        $object = $this->resource($uri);
        $object = $object[$uri];

        $prefix = [
            'lom'     => 'http://ltsc.ieee.org/rdf/lomv1p0/lom#',
            'dcterms' => 'http://purl.org/dc/terms/',
            'aprende' => 'http://www.igualproject/rdf/aprende#',
            'vcard'   => 'http://www.w3.org/2001/vcard-rdf/3.0#',
            'lomvoc'  => 'http://ltsc.ieee.org/rdf/lomv1p0/vocabulary#'
        ];

        $fields = [
            'dcterms:title',
            'dcterms:description',
            'dcterms:language',
            'dcterms:type',
            'dcterms:format',
            'aprende:thumb',
            'aprende:visits',
            'aprende:saves',
            'aprende:tags',
            'lom:educationalContext'
        ];

        foreach ($fields as $field) {
            $f = explode(':', $field);
            $o = $object[$prefix[$f[0]] . $f[1]];
            $d = array_map(function($e) {
                return $e['value'];
            }, $object[$prefix[$f[0]] . $f[1]]);
            $data[$f[1]] = implode(', ', $d);
        }

        $oercommons_courses = 'https://www.oercommons.org/courses/';
        $slugify = new Slugify();
        $data['resource'] = $oercommons_courses . $slugify->slugify($data['title']);
        $data['resource_view'] = $data['resource'] . '/view';

        $data['thumb'] = preg_match("/^http/", $data['thumb'])
            ? $data['thumb']
            : '';

        foreach ($object[$prefix['lom'] . 'classification'] as $classification) {
            $resource_uri = $classification['value'];
            $resource = $this->resource($resource_uri);
            $resource = $resource[$resource_uri];
            $data['subjects'][] = $resource[$prefix['lom'] . 'classificationKeyword'][0]['value'];
        }

        $data['subjects'] = implode(', ', array_unique($data['subjects'], SORT_STRING));

        foreach ($object[$prefix['lom'] . 'lifeCycleContribution'] as $contribution) {
            $resource_uri = $contribution['value'];
            $resource = $this->resource($resource_uri);
            $resource = $resource[$resource_uri];
            $key = $resource[$prefix['lom'] . 'contributionRole'][0]['value'] == $prefix['lomvoc'] . 'Role-author'
                ? 'author'
                : 'publisher';
            $contribution_uri = $resource[$prefix['lom'] . 'contributionEntity'][0]['value'];
            $contribution = $this->resource($contribution_uri);
            $contribution = $contribution[$contribution_uri];
            $data[$key][] = $contribution[$prefix['vcard'] . 'N'][0]['value'];
        }

        $data['author'] = isset($data['author'])
            ? implode(', ', array_unique($data['author'], SORT_STRING))
            : '';

        $data['publisher'] = isset($data['publisher'])
            ? implode(', ', array_unique($data['publisher'], SORT_STRING))
            : '';

        return $data;
        */
    }

    /**
    * Get objects by categories
    */
    public function getObjectsByCategory($categories, $numrecs = 10)
    {
        $filters = array_map(function($e) {
            return '?keyword = "' . $e . '"';
        }, $categories);

        $filter = implode(' || ', $filters);

        $results = $this->select('
            PREFIX lom:     <http://ltsc.ieee.org/rdf/lomv1p0/lom#>

            SELECT DISTINCT ?object WHERE {
              ?object a lom:LearningObject ;
                        lom:classification ?classification .
              ?classification lom:classificationKeyword ?keyword .
              FILTER (' . $filter . ')
            }
            LIMIT ' . (string) $numrecs . '
        ');

        return array_map(function($e) {
            return $e['object']['value'];
        }, $results);
    }

    /**
    * Execute select queries in marmotta
    */
    public function select($query) {
        $url = $this->config['url'] . self::$PATH_QUERY_SERVICE;
        try {
            $client = new Client();
            $request = $client->post($url, [
                "Accept" => "application/json; charset=utf-8",
                "User-Agent" => "Aprende/1.0",
                "timeout" => 5, // Response timeout
                "connect_timeout" => 5 // Connection timeout
            ], [
                'query' => $query
            ]);
            if($this->config['username'] && $this->config['password']) {
                $request->setAuth($this->config['username'], $this->config['password']);
            }
            $response = $request->send();
            $result = $response->getBody(true);
            return $result
                ? json_decode($result, true)['results']['bindings']
                : false;
        } catch(BadResponseException $ex) {
            throw new MarmottaClientException("error evaluating SPARQL Select Query $query; " . $ex->getResponse()->getReasonPhrase());
        }
    }

    /**
    * Execute update queries in marmotta
    */
    public function update($update) {
        $url = $this->config['url'] . self::$PATH_UPDATE_SERVICE;
        try {
            $client = new Client();
            $request = $client->post($url, [
                "Accept" => "application/json; charset=utf-8",
                "User-Agent" => "Aprende/1.0",
                "timeout" => 5, // Response timeout
                "connect_timeout" => 5 // Connection timeout
            ], [
                'update' => $update
            ]);
            if($this->config['username'] && $this->config['password']) {
                $request->setAuth($this->config['username'], $this->config['password']);
            }
            $response = $request->send();
        } catch(BadResponseException $ex) {
            throw new MarmottaClientException("error evaluating SPARQL Update Query $query; " . $ex->getResponse()->getReasonPhrase());
        }
    }

    /**
    * Get Resource in rdf+json
    */
    public function resource($resource)
    {
        $url = $this->config['url'] . self::$PATH_RESOURCE_SERVICE . '?uri=' . urlencode($resource);
        try {
            $client = new Client();
            $request = $client->get($url, [
                "Accept" => "application/rdf+json; charset=utf-8",
                "User-Agent" => "Aprende/1.0",
                "timeout" => 5, // Response timeout
                "connect_timeout" => 5 // Connection timeout
            ]);
            if($this->config['username'] && $this->config['password']) {
                $request->setAuth($this->config['username'], $this->config['password']);
            }
            $response = $request->send();
            $result = $response->getBody(true);
            return $result
                ? json_decode($result, true)
                : false;
        } catch(BadResponseException $ex) {
            throw new MarmottaClientException("error evaluating Resource $resource; " . $ex->getResponse()->getReasonPhrase());
        }
    }

    /**
     * Insert all rfds in Marmotta
     */
    public function insertRDFS()
    {
        // RDFs
        $PATH = __DIR__ . '/../../../../docs/RDFs/rdfs/';

        $files = array_diff(scandir($PATH), ['.', '..']);

        foreach ($files as $file) {
            $rdf = file_get_contents($PATH . $file);
            $this->update($rdf);
        }
    }

}
