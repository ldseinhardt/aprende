<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende\Models;

use Silex\Application;

class Model
{

    /**
    * Database link
    */
    protected $db;

    /**
    * Data
    */
    protected $data;

    /**
    * Table name
    */
    protected $table;

    /**
    * Secret value to hash password
    */
    protected $secret;

    /**
    * Inicialize class
    */
    function __construct($db, $table, $data = null)
    {
        $this->db = $db;

        $this->table = $table;

        $this->data = $data;
    }

    /**
    * Set data to object
    */
    public function setData($data)
    {
        if ($data) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, $this->data)) {
                    $this->data[$key] = ($key === 'password')
                        ? $this->hashPassword($value)
                        : $value;
                }
            }
        }
        return $this;
    }

    /**
    * Return hash password
    */
    protected function hashPassword($password)
    {
        return hash('sha256', $this->secret . $password);
    }

}
