<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

namespace Aprende\Models;

use Silex\Application;

class User extends Model
{

    /**
    * Inicialize class
    */
    function __construct($db, $data = null)
    {
        parent::__construct($db, 'users', [
            'id'                     => null,
            'first_name'             => null,
            'last_name'              => null,
            'email'                  => null,
            'password'               => null,
            'password_token'         => null,
            'password_token_expires' => null,
            'active'                 => null,
            'date'                   => null
        ]);

        $this->setData($data);

        $this->secret = 'aprende_secret';
    }

    /**
    * Add user and return user id
    */
    public function add($data = null)
    {
        $this->setData($data);

        if ($this->getUserIdByEmail()) {
            return 0;
        }

        $this->data['date'] = date('Y-m-d H:i:s');


        $data = array_filter($this->data, function($e) {
            return $e !== null;
        });

        $this->db->insert($this->table, $data);

        return $this->db->lastInsertId();
    }

    /**
    * Verify email and password to login
    */
    public function auth($email = null, $password = null)
    {
        if ($email) {
            $this->data['email'] = $email;
        }

        if ($password) {
            $this->data['password'] = $this->hashPassword($password);
        }

        return $this->db->fetchAssoc("
            SELECT
                id,
                first_name,
                last_name,
                email
            FROM
                users
            WHERE
                email = :email and password = :password
        ", $this->data);
    }

    /**
    * Return user id by email
    */
    private function getUserIdByEmail($email = null)
    {
        if ($email) {
            $this->data['email'] = $email;
        }
        $query = $this->db->fetchAssoc("
            SELECT
                id
            FROM
                users
            WHERE
                email = :email
        ", $this->data);
        return $query ? $query['id'] : 0;
    }

}
