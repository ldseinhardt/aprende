<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 * Recommender by Henrique Lemos
 */

namespace Aprende\Models;

use Silex\Application;

class View extends Model
{

    /**
    * Inicialize class
    */
    function __construct($db, $data = null)
    {
        parent::__construct($db, 'views', [
            'user_id'   => null,
            'object_id' => null,
            'date'      => null
        ]);

        $this->setData($data);
    }

    /**
    * Add
    */
    public function add($data = null)
    {
        $this->setData($data);

        $this->data['date'] = date('Y-m-d H:i:s');

        if (!$this->contains()) {
            $this->db->insert($this->table, $this->data);
        }

        return $this;
    }

    /**
    * Get
    */
    public function get($data = null)
    {
        $this->setData($data);

        $query = $this->db->fetchAssoc("
            SELECT
                COUNT(object_id) as n
            FROM
                views
            WHERE
                object_id = :object_id
        ", $this->data);

        return $query ? (int) $query['n'] : 0;
    }

    /**
    * Return user objects with views
    */
    public function getObjects($user_id = null)
    {
        if ($user_id) {
            $this->data['user_id'] = $user_id;
        }

        $query = $this->db->fetchAll("
            SELECT
                object_id as id
            FROM
                views
            WHERE
                user_id = :user_id
            GROUP BY object_id
        ", $this->data);

        return $query ? array_map(function($e) {
            return $e['id'];
        }, $query) : [];
    }

    /**
    * Return objects with the most views
    */
    public function getTopObjects($limit = 10)
    {
        $query = $this->db->fetchAll("
            SELECT
                object_id as id
            FROM
                views
            GROUP BY object_id
            ORDER BY COUNT(object_id) DESC
            LIMIT {$limit}
        ", $this->data);

        return $query ? array_map(function($e) {
            return $e['id'];
        }, $query) : [];
    }

    /**
    * Return user categories with views
    */
    public function getCategories($user_id = null)
    {
        if ($user_id) {
            $this->data['user_id'] = $user_id;
        }

        $query = $this->db->fetchAll("
            SELECT
                categories_objects.category
            FROM
                views
                LEFT JOIN categories_objects
                    ON (views.object_id = categories_objects.object_id)
            WHERE
                views.user_id = :user_id
            GROUP BY categories_objects.category
        ", $this->data);

        return $query ? array_map(function($e) {
            return $e['category'];
        }, $query) : [];
    }

    /**
    * Return object views
    */
    public function getViews($recs)
    {
        $filter = implode(' or ', array_map(function() {
            return 'object_id = ?';
        }, $recs));

        $rows = $this->db->fetchAll("
            SELECT
                object_id as id,
                COUNT(object_id) as n
            FROM
                views
            WHERE
                {$filter}
            GROUP BY object_id
        ", $recs);

        $recs_views = [];

        foreach ($rows as $row) {
            $recs_views[$row['id']] = $row['n'];
        }

        $result = [];

        foreach ($recs as $rec) {
            $result[$rec] = $recs_views && array_key_exists($rec, $recs_views)
                ? $recs_views[$rec]
                : 0;
        }

        return $result;
    }

    /**
    * Check view
    */
    private function contains()
    {
        $query = $this->db->fetchAssoc("
            SELECT
                date
            FROM
                views
            WHERE
                user_id = :user_id and object_id = :object_id and date = :date
        ", $this->data);
        return $query !== false;
    }

}
