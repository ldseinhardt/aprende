<?php
/**
 * Aprende
 * Author: Luan Einhardt
 * Contact: ldseinhardt@gmail.com
 */

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/configs.php';

use Silex\Application;
use Silex\Provider\HttpCacheServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\HttpFoundation\Request;

// Inicialize app
$app = new Application();

// HTTP Cache
$app->register(new HttpCacheServiceProvider(), [
    'http_cache.cache_dir' => __DIR__ . '/../temp/cache/http_cache/',
    'http_cache.esi' => null
]);

// Twig
$app->register(new TwigServiceProvider(), [
    'twig.options' => [
        'cache' => __DIR__ . '/../temp/cache/twig/'
    ],
    'twig.path' => __DIR__ . '/../src/',
    'dump' => true
]);

// Database
$app->register(new DoctrineServiceProvider(), [
    'db.options' => $database
]);

// Session
$app->register(new SessionServiceProvider(), [
    'session.storage.options' => [
        'cookie_lifetime' => 31536000
    ]
]);

// Validator
$app->register(new ValidatorServiceProvider());

$app['marmotta_config'] = $marmotta;

// Add routes
$app->mount('/', new Aprende\Aprende());

// Static (Client)
$app->get('/{file}', function($file) use($app) {
    $filename = __DIR__ . '/' . $file;

    if (!file_exists($filename)) {
        return $app->abort(404);
    }

    $headers = [];

    switch (pathinfo($filename, PATHINFO_EXTENSION)) {
        case 'css':
            $headers['content-type'] = 'text/css; charset=utf-8';
            break;
        case 'html':
            $headers['content-type'] = 'text/html; charset=utf-8';
            break;
        case 'js':
            $headers['content-type'] = 'application/javascript; charset=utf-8';
            break;
    }

    return $app->sendFile($filename, 200, $headers);
})
    ->assert('file', '.+')
    ->bind('files');

// Errors
$app->error(function(Exception $e, $code) use($app) {
    if (!$app['debug']) {
        return $app->redirect('/');
    }
});

// Debug
$app['debug'] = $debug;

// Request export
$app['request'] = Request::createFromGlobals();

if ($app['debug']) {
    $app->run();
} else {
    $app['http_cache']->run();
}
