-- Aprende
-- Author: Luan Einhardt
-- ldseinhardt@gmail.com

DROP DATABASE IF EXISTS aprende;

CREATE DATABASE aprende;

USE aprende;

-- Users table
CREATE TABLE IF NOT EXISTS `users` (
  `id`                     INT          NOT NULL AUTO_INCREMENT,
  `first_name`             VARCHAR(32)  NOT NULL,
  `last_name`              VARCHAR(64)  NOT NULL,
  `email`                  VARCHAR(255) NOT NULL UNIQUE,
  `password`               VARCHAR(64)  NOT NULL,
  `password_token`         VARCHAR(64)  NULL,
  `password_token_expires` TIMESTAMP    NULL,
  `active`                 TINYINT(1)   NOT NULL DEFAULT 1,
  `date`                   TIMESTAMP    NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Categories x Objects table
-- object_id is URI
CREATE TABLE IF NOT EXISTS `categories_objects` (
  `category`  VARCHAR(255) NOT NULL,
  `object_id` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`category`, `object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Views Table
-- object_id is URI
CREATE TABLE IF NOT EXISTS `views` (
  `user_id`   INT          NOT NULL,
  `object_id` VARCHAR(255) NOT NULL,
  `date`      TIMESTAMP    NOT NULL,
  PRIMARY KEY (`user_id`, `object_id`, `date`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
