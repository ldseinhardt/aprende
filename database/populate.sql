-- Aprende
-- Author: Luan Einhardt
-- ldseinhardt@gmail.com

USE aprende;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `active`, `date`) VALUES
  (1, 'Luan', 'Einhardt', 'ldseinhardt@gmail.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 1, CURRENT_TIMESTAMP);
