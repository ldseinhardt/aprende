fs = require('fs');

//rdfs.txt
var file = process.argv[2];

(function(fs, file) {

  if (!file) {
    return;
  }

  function pad(n, width) {
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
  }

  fs.readFile(file, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }

    var datas = data.split(/pagesTest\/[0-9]+.html\n/);

    datas.shift();

    if (datas && datas.length) {
      datas.forEach(function(value, i) {
        var filename = 'rdfs/' + pad(i + 1, 4) + '.rdf';
        console.log(filename);
        fs.writeFile(filename, value, function(err) {
            if(err) {
              return console.log(err);
            }
        });
      });
    }
  });

})(fs, file);
