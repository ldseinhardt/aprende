/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.importador;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.marmotta.client.ClientConfiguration;
import org.apache.marmotta.client.clients.ImportClient;
import org.apache.marmotta.client.clients.ResourceClient;
import org.apache.marmotta.client.clients.SPARQLClient;
import org.apache.marmotta.client.exception.MarmottaClientException;
import org.apache.marmotta.client.model.meta.Metadata;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.Rio;

/**
 *
 * @author Henrique
 */
public class Importador {
    public static void main(String[] args) throws FileNotFoundException, IOException, URISyntaxException, MarmottaClientException{
        String path = "C:\\Users\\Henrique\\Downloads\\RDFs\\rdfs\\";

        ClientConfiguration configuration = new ClientConfiguration("http://aqualung:8080/marmotta/");
        SPARQLClient client = new SPARQLClient(configuration);

        for(int i = 2; i <= 31; i++){
            String content = new String(Files.readAllBytes(Paths.get(path+i+".rdf")));
            //System.out.println(content);
            try {
                    client.update(content);
            } catch (Exception e) {
                    System.err.println("Update query failed: " + e.getMessage());
            }
        }
    }  
}
