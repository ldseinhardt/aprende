var gulp = require('gulp'),
    htmlmin = require('gulp-htmlmin'),
    less = require('gulp-less'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');

// Paths
var paths = {
  src: {
    views: 'src/views/',
    assets: 'src/assets/'
  },
  dist: {
    views: '../back-end/src/Aprende/Views/',
    assets: '../back-end/web/assets/'
  }
};

// Copy html
gulp.task('copy_html', function() {
  return gulp.src(paths.src.views + '**/*.html')
  .pipe(htmlmin({ collapseWhitespace: true }))
  .pipe(gulp.dest(paths.dist.views));
});

// Copy fonts
gulp.task('copy_fonts_bootstrap', function() {
  return gulp.src([
    'node_modules/bootstrap/dist/fonts/*.{eot,ttf,woff,woff2,eof,svg}'
  ])
  .pipe(gulp.dest(paths.dist.assets + 'fonts'));
});

// Copy fonts
gulp.task('copy_fonts_awesome', function() {
  return gulp.src([
    'node_modules/font-awesome/fonts/*.{eot,ttf,woff,woff2,otf,svg}'
  ])
  .pipe(gulp.dest(paths.dist.assets + 'fonts'));
});

// Styles
gulp.task('styles', function() {
  return gulp.src([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/font-awesome/css/font-awesome.css',
    paths.src.assets + 'styles/main.less'
  ])
  .pipe(less())
  .pipe(concat('main.css'))
  .pipe(gulp.dest(paths.dist.assets + 'styles'))
  .pipe(rename({ suffix: '.min' }))
  .pipe(cssnano())
  .pipe(gulp.dest(paths.dist.assets + 'styles'));
});

// Scripts
gulp.task('scripts', function() {
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/mark.js/dist/jquery.mark.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/vue/dist/vue.min.js',
    'node_modules/vue-resource/dist/vue-resource.min.js',
    paths.src.assets + 'scripts/**/*.js'
  ])
  .pipe(concat('main.js'))
  .pipe(gulp.dest(paths.dist.assets + 'scripts'))
  .pipe(rename({ suffix: '.min' }))
  .pipe(uglify())
  .pipe(gulp.dest(paths.dist.assets + 'scripts'));
});

// Images
gulp.task('images', function() {
  return gulp.src(paths.src.assets + 'images/**/*')
  .pipe(cache(imagemin({
    optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
  .pipe(gulp.dest(paths.dist.assets + 'images'));
});

// Clean
gulp.task('clean', function() {
  return del([
    paths.dist.views,
    paths.dist.assets
  ], {
    force: true
  });
});

// Default task
gulp.task('default', ['clean'], function() {
  return gulp.start(
    'copy_html',
    'copy_fonts_bootstrap',
    'copy_fonts_awesome',
    'styles',
    'scripts',
    'images'
  );
});

// Watch
gulp.task('watch', function() {

  // Watch .html files
  gulp.watch(paths.src.views + '**/*.html', ['copy_html']);

  // Watch .less files
  gulp.watch(paths.src.assets + 'styles/**/*.less', ['styles']);

  // Watch .js files
  gulp.watch(paths.src.assets + 'scripts/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch(paths.src.assets + 'images/**/*', ['images']);

  // Create LiveReload server
  livereload.listen();

  // Watch any files in dist/, reload on change
  gulp.watch([
    paths.dist.views + '**',
    paths.dist.assets + '**'
  ]).on('change', livereload.changed);

});
