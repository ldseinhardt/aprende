(function(global, $) {
  // search highlight
  var keyword = '';
  if ($('.search-bar .search-input').length) {
    keyword = $('.search-bar .search-input').val();
  }

  if (keyword) {
    $('.results').mark(keyword, {
      element: 'span',
      className: 'highlight'
    });
  }

  // views
  var View = function(url, object_id, categories) {
    this.url = url;
    this.object_id = object_id;
    this.categories = categories;
    return this;
  };

  View.prototype.add = function() {
    $.post(this.url, {
      object_id: this.object_id,
      categories: this.categories
    });
    return this;
  };

  View.prototype.show = function(callback) {
    $.get(this.url, {
      object_id: this.object_id
    }, function(response) {
      if (callback && response.status) {
        callback(response.data);
      }
    }, 'json');
    return this;
  }

  // recommender
  var Recommender = function(url) {
    this.url = url;
    return this;
  };

  Recommender.prototype.show = function(callback) {
    $.get(this.url, function(response) {
      if (callback && response.status && response.data) {
        callback(response.data);
      }
    }, 'json');
    return this;
  };

  // exports

  global.View = View;
  global.Recommender = Recommender;

})(this, jQuery);
